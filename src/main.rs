extern crate hyper;
extern crate iron;
extern crate mount;
extern crate router;
extern crate bodyparser;
extern crate persistent;
extern crate url;
extern crate urlencoded;
extern crate rustc_serialize;
extern crate jwt;
extern crate crypto;
extern crate time;

use iron::prelude::*;

use mount::Mount;

use persistent::Read;

use router::Router;

use std::collections::HashMap;
use std::path::PathBuf;

mod auth;
mod v0_01;
mod config;
mod handlers;

fn main() {

    // TEMP ROUTER
    let mut router = Router::new();
    router.get("/", |_: &mut Request| Ok(Response::with("Welcome to the API")));

    // Initialize Mounts
    let mut mount = Mount::new();
    mount.mount("/", router);
    mount.mount("/v0.01", v0_01::initialize_router());
    mount.mount("/auth", auth::initialize_router());

    let mut chain = Chain::new(mount);
    let config_manager = config::ConfigManager::new();
    let config = config_manager.settings.clone();
    chain.link_before(config_manager);
    let secret = config.find("secret").unwrap().as_string().unwrap();
    chain.link_before(auth::token::TokenManager::new(secret.to_string(), "https://localhost:6767".to_string()));
    chain.link_before(auth::CurrentUserExtractor);
    chain.link_before(Read::<bodyparser::MaxBodyLength>::one(10*1024*1024));

    let private_key_path_buf = PathBuf::from("keys/devkey.pem");
    let public_cert_path_buf = PathBuf::from("keys/devserver.crt");
    Iron::new(chain)
        .https("0.0.0.0:6767", public_cert_path_buf, private_key_path_buf).unwrap();
}
