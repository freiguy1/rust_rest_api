use ::handlers::unsecured::{ RespAction, Bundle };
use ::handlers::{ ActionError };
use std::collections::HashMap;

pub struct GetAll;

impl RespAction for GetAll {
    type TResp = GetAllResponse;

    // Step 1
    fn validate(&self, query_params: &::router::Params) -> HashMap<&str, &str> {
        HashMap::new()
    }

    // Step 2
    fn handle(&self,
              bundle: Bundle) -> Result<Self::TResp, ActionError> {
        let teams = vec![
            Team { num: 1, league_id: 1, name: "Team 1".to_string() },
            Team { num: 2, league_id: 1, name: "Team 2".to_string() },
            Team { num: 3, league_id: 1, name: "Team 3".to_string() },
            Team { num: 4, league_id: 1, name: "Team 4".to_string() },
            Team { num: 1, league_id: 2, name: "Team 2.1".to_string() }];
        Ok(GetAllResponse{ teams: teams })
    }
}


#[derive(RustcEncodable)]
struct GetAllResponse {
    teams: Vec<Team>
}

#[derive(RustcEncodable)]
struct Team {
    num: usize,
    league_id: usize,
    name: String
}
