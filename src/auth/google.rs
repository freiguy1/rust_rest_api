use config::Config;
use hyper::header::{ Authorization, Bearer, ContentType };
use hyper::Client;
use iron::prelude::*;
use iron::{ Url, status };
use iron::modifiers::Redirect;
use router::Router;
use rustc_serialize::json;
use std::io::Read;
use url::percent_encoding::{ utf8_percent_encode, DEFAULT_ENCODE_SET };
use urlencoded::UrlEncodedQuery;

pub fn initialize_router(router: &mut Router) {
    router.get("/google/callback", google_callback);
    router.get("/google", google_login_redirect);
}


fn google_login_redirect(req: &mut Request) -> IronResult<Response> {
    let config = req.extensions.get::<::config::ConfigManager>().unwrap();
    let client_id = try!(config.fetch_string(&["google", "client_id"]));
    let generic_url = req.url.clone().into_generic_url();
    let host = generic_url.host().unwrap();
    let port = generic_url.port().unwrap();
    let url_port = if port == 443 { "".to_string() } else { format!(":{}", port) };
    let redirect_uri = format!("https://{}{}/auth/google/callback", host, url_port);
    let encoded_redirect_uri = utf8_percent_encode(&redirect_uri, DEFAULT_ENCODE_SET);
    let url = Url::parse(&format!("https://accounts.google.com/o/oauth2/auth?response_type=code&client_id={}&scope=email%20profile&access_type=offline&redirect_uri={}", client_id, encoded_redirect_uri)).unwrap();
    Ok(Response::with((status::Found, Redirect(url.clone()))))
}

fn google_callback(req: &mut Request) -> IronResult<Response> {
    let mut client_id;
    let mut client_secret;
    {
        let config = req.extensions.get::<::config::ConfigManager>().unwrap();
        client_id = try!(config.fetch_string(&["google", "client_id"])).to_string();
        client_secret = try!(config.fetch_string(&["google", "client_secret"])).to_string();
    }

    let code = match get_code_from_request(req) {
        Ok(c) => c,
        Err(e) => return Ok(Response::with((status::BadRequest, e)))
    };
    let token = match get_token_from_code(req, code, &client_id, &client_secret) {
        Ok(t) => t,
        Err(e) => return Ok(Response::with((status::BadRequest, e)))
    };
    let user_data = match get_user_data_with_token(token) {
        Ok(ud) => ud,
        Err(e) => return Ok(Response::with((status::BadRequest, e)))
    };

    // Fetch user. If not found, create.
    let user_id = 2;
    Ok(::auth::auth_success_token_response(req, user_id))
}

fn get_token_from_code(req: &mut Request, code: String, client_id: &String, client_secret: &String) -> Result<::auth::TokenResponse, String> {
    let google_token_uri = "https://www.googleapis.com/oauth2/v3/token";
    let generic_url = req.url.clone().into_generic_url();
    let host = generic_url.host().unwrap();
    let port = generic_url.port().unwrap();
    let url_port = if port == 443 { "".to_string() } else { format!(":{}", port) };
    let redirect_uri = format!("https://{}{}/auth/google/callback", host, url_port);
    let grant_type = "authorization_code";

    let query_string = format!("code={}&client_id={}&client_secret={}&redirect_uri={}&grant_type={}",
                               code,
                               client_id,
                               client_secret,
                               redirect_uri,
                               grant_type);

    let client = Client::new();
    let mut response = client
        .post(google_token_uri)
        .body(&query_string)
        .header(ContentType::form_url_encoded())
        .send().unwrap();
    let mut response_body = String::new();
    if let Err(e) = response.read_to_string(&mut response_body) {
        return Err(e.to_string());
    }
    match json::decode(&response_body) {
        Ok(decoded) => Ok(decoded),
        Err(_) => Err("Unable to authenticate with Google".to_string())
    }
}

fn get_code_from_request(req: &mut Request) -> Result<String, String> {
    match req.get::<UrlEncodedQuery>() {
        Ok(ref hashmap) => {
            let code = if hashmap.contains_key(&"code".to_string())
                && hashmap.get(&"code".to_string()).unwrap().len() == 1 {
                hashmap.get(&"code".to_string()).unwrap()[0].clone()
            } else {
                return Err("Query string does not contain correct members".to_string());
            };
            Ok(code)
        },
        Err(ref e) => Err(format!("Problem parsing query string. Error: {:?}", e))
    }
}

fn get_user_data_with_token(token: ::auth::TokenResponse) -> Result<::auth::UserData, String> {
    let google_user_data_uri = "https://www.googleapis.com/userinfo/v2/me";
    let client = Client::new();
    let mut response = client
        .get(google_user_data_uri)
        .header(Authorization(Bearer { token: token.access_token.clone() }))
        .send()
        .unwrap();
    let mut response_body = String::new();
    if let Err(e) = response.read_to_string(&mut response_body) {
        return Err(e.to_string());
    }
    match json::decode(&response_body) {
        Ok(decoded) => Ok(decoded),
        Err(_) => Err("Unable to authenticate with Google".to_string())
    }
}
