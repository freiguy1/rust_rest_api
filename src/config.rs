use std::fs::File;
use std::sync::Arc;
use iron::{ status, typemap, BeforeMiddleware };
use iron::prelude::*;
use rustc_serialize::json::Json;

pub struct ConfigManager {
    pub settings: Arc<Json>
}

impl ConfigManager {
    pub fn new() -> ConfigManager {

        let mut file = match File::open("config.json") {
            Ok(f) => f,
            Err(_) => panic!("config.json does not exist in current directory")
        };

        let json = match Json::from_reader(&mut file) {
            Ok(j) => j,
            Err(_) => panic!("Unable to read and parse from config.json")
        };

        ConfigManager { settings: Arc::new(json) }
    }
}

impl typemap::Key for ConfigManager { type Value = Arc<Json>; }

impl BeforeMiddleware for ConfigManager {
    fn before(&self, req: &mut Request) -> IronResult<()> {
        req.extensions.insert::<ConfigManager>(self.settings.clone());
        Ok(())
    }
}

#[derive(Debug)]
pub enum ConfigError {
    PathNotFound,
    WrongTypeAtPath
}

impl ::std::fmt::Display for ConfigError {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::std::fmt::Debug::fmt(self, f)
    }
}

impl ::std::error::Error for ConfigError {
    fn description(&self) -> &str {
        match *self {
            ConfigError::PathNotFound => "Path Not Found",
            ConfigError::WrongTypeAtPath => "Wrong Type at Path"
        }
    }
}

pub trait Config {
    fn fetch_string(&self, path: &[&str]) -> Result<&str, ConfigError>;
    fn fetch_u64(&self, path: &[&str]) -> Result<u64, ConfigError>;
}

impl Config for Json {
    fn fetch_string(&self, path: &[&str]) -> Result<&str, ConfigError> {
        let result = match self.find_path(path) {
            Some(j) => j,
            None => return Err(ConfigError::PathNotFound)
        };

        if let Some(s) = result.as_string() {
            Ok(s)
        } else {
            Err(ConfigError::WrongTypeAtPath)
        }
    }

    fn fetch_u64(&self, path: &[&str]) -> Result<u64, ConfigError> {
        let result = match self.find_path(path) {
            Some(j) => j,
            None => return Err(ConfigError::PathNotFound)
        };

        if let Some(u) = result.as_u64() {
            Ok(u)
        } else {
            Err(ConfigError::WrongTypeAtPath)
        }
    }
}

impl From<ConfigError> for IronError {
    fn from(e: ConfigError) -> IronError {
        IronError::new(e, status::InternalServerError)
    }
}
