use router::Router;
use iron::prelude::*;
use iron::status;

mod get_all;
mod get_by_league_and_num;

pub fn initialize_router(router: &mut Router) {
    router.get("/teams/", ::handlers::unsecured::RespHandler::new(get_all::GetAll));
    router.get("/leagues/:league_id/teams/:team_num", ::handlers::unsecured::RespHandler::new(get_by_league_and_num::GetByLeagueAndNum));
}

