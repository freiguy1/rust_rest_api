
use router::Router;

use iron::headers::{ Authorization, Bearer };
use iron::prelude::*;
use iron::{ BeforeMiddleware, typemap, status };


mod google;
mod usernamepassword;
pub mod token;

pub fn initialize_router() -> Router {
    let mut router = Router::new();
    google::initialize_router(&mut router);
    usernamepassword::initialize_router(&mut router);
    router
}

fn auth_success_token_response(req: &Request, user_id: usize) -> Response {
    let token_manager = req.extensions.get::<token::TokenManager>().unwrap();
    let token = token_manager.encode(user_id);
    Response::with((status::Ok, token))
}

#[derive(RustcDecodable, Debug)]
struct UserData {
    family_name: String,
    given_name: String,
    name: String,
    picture: String,
    email: String
}

#[derive(RustcDecodable, Debug)]
struct TokenResponse {
    access_token: String,
    expires_in: usize,
    token_type: String,
    refresh_token: Option<String>
}

#[derive(Clone)]
pub struct CurrentUser {
    pub user_id: usize
}

impl typemap::Key for CurrentUser { type Value = CurrentUser; }

pub struct CurrentUserExtractor;

impl BeforeMiddleware for CurrentUserExtractor {
    fn before(&self, req: &mut Request) -> IronResult<()> {
        let bearer_token = match req.headers.get::<Authorization<Bearer>>() {
            Some(auth) => auth.token.clone(),
            None => return Ok(())
        };
        let user_id = match req.extensions.get::<token::TokenManager>().unwrap().decode(bearer_token) {
            Some(id) => req.extensions.insert::<CurrentUser>(CurrentUser { user_id: id }),
            None => return Ok(())
        };
        Ok(())
    }
}
