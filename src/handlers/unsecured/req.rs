use handlers::{
    parse_request_from_json,
    create_invalid_request_response,
    ActionError };
use handlers::unsecured::{ BundleWithReq, extract_current_user };
use iron::{ Handler, status };
use iron::prelude::*;
use iron::mime::Mime;
use router::Router;
use rustc_serialize::Decodable;
use std::collections::HashMap;
use std::any::Any;

pub trait ReqAction: Send + Sync + Any {
    type TReq: 'static + Decodable + Any + Clone;

    // Step 1
    fn validate(&self,
                query_params: &::router::Params,
                transfer_object: &Self::TReq) -> HashMap<&str, &str>;

    // Step 2
    fn handle(&self,
              bundle: BundleWithReq<Self::TReq>) -> Result<Option<usize>, ActionError> ;
}

pub struct ReqHandler<A: ReqAction> {
    action: A
}

impl<A: ReqAction> ReqHandler<A> {
    pub fn new(action: A) -> ReqHandler<A> {
        ReqHandler {
            action: action
        }
    }
}

impl<A: 'static + ReqAction> Handler for ReqHandler<A> {
    fn handle(&self, req: &mut Request) -> IronResult<Response> {
        // Get query params
        let query_params = req.extensions.get::<Router>().unwrap().clone();

        // Get configuration
        let config = req.extensions.get::<::config::ConfigManager>().unwrap().clone();

        // Get user
        let current_user_opt = extract_current_user(req);

        // Parse request from json
        let transfer_object: A::TReq = match parse_request_from_json(req) {
            Ok(parsed) => parsed,
            Err(err_response) => return err_response
        };

        // Validate request
        let errors = self.action.validate(&query_params, &transfer_object);
        if let Err(err_response) = create_invalid_request_response(errors) {
            return err_response;
        }

        let bundle = BundleWithReq {
            transfer_object: transfer_object,
            query_params: query_params,
            current_user: current_user_opt,
            config: config
        };

        match try!(self.action.handle(bundle)) {
            Some(id) => {
                let response = format!("{{ \"id\": {} }}", id);
                let content_type = "application/json".parse::<Mime>().unwrap();
                Ok(Response::with((content_type, status::Ok, response)))
            },
            None => Ok(Response::with(status::Ok))
        }
    }
}
