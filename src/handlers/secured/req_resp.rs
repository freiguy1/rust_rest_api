use handlers::{
    parse_request_from_json,
    create_invalid_request_response,
    authorize_current_user_opt,
    ActionError };
use handlers::secured::{ BundleWithReq, extract_current_user };
use iron::{ Handler, status };
use iron::prelude::*;
use iron::mime::Mime;
use router::Router;
use rustc_serialize::json::encode as json_encode;
use rustc_serialize::{ Decodable, Encodable };
use std::any::Any;
use std::collections::HashMap;

pub trait ReqRespAction: Send + Sync + Any {
    type TReq: 'static + Decodable + Any + Clone;
    type TResp: 'static +  Encodable;

    // Step 1
    fn authorize(&self,
                 query_params: &::router::Params,
                 current_user: &::auth::CurrentUser) -> bool {
        true
    }

    // Step 2
    fn validate(&self,
                query_params: &::router::Params,
                transfer_object: &Self::TReq) -> HashMap<&str, &str>;

    // Step 3
    fn handle(&self,
              bundle: BundleWithReq<Self::TReq>) -> Result<Self::TResp, ActionError>;
}

pub struct ReqRespHandler<A: ReqRespAction> {
    action: A
}

impl<A: ReqRespAction> ReqRespHandler<A> {
    pub fn new(action: A) -> ReqRespHandler<A> {
        ReqRespHandler {
            action: action
        }
    }
}

impl<A: 'static + ReqRespAction> Handler for ReqRespHandler<A> {
    fn handle(&self, req: &mut Request) -> IronResult<Response> {
        // Get query params
        let query_params = req.extensions.get::<Router>().unwrap().clone();

        // Get configuration
        let config = req.extensions.get::<::config::ConfigManager>().unwrap().clone();

        // Get user
        let current_user = match extract_current_user(req) {
            Ok(user) => user,
            Err(err_response) => return err_response
        };

        // Authorize
        let current_user = Some(current_user);
        if let Err(err_response) = authorize_current_user_opt(
            &current_user,
            |cu| self.action.authorize(&query_params, cu)) {
            return err_response;
        }
        let current_user = current_user.unwrap();

        // Parse request from json
        let transfer_object: A::TReq = match parse_request_from_json(req) {
            Ok(parsed) => parsed,
            Err(err_response) => return err_response
        };

        // Validate request
        let errors = self.action.validate(&query_params, &transfer_object);
        if let Err(err_response) = create_invalid_request_response(errors) {
            return err_response;
        }

        let bundle = BundleWithReq {
            transfer_object: transfer_object,
            query_params: query_params,
            current_user: current_user,
            config: config
        };

        let response_body = try!(self.action.handle(bundle));
        let encoded_response_body = json_encode(&response_body).unwrap();
        let content_type = "application/json".parse::<Mime>().unwrap();
        Ok(Response::with((content_type, status::Ok, encoded_response_body)))
    }
}
