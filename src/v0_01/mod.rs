
use router::Router;
use iron::prelude::*;
use iron::status;

mod team;

pub fn initialize_router() -> Router {
    let mut router = Router::new();
    router.get("/", |_: &mut Request| {
        ::std::thread::sleep_ms(10000);
        Ok(Response::with((status::Ok, "Welcome to api version 0.01!")))
    });

//    router.post("/createleague", ::handler::ReqHandler::new(league::CreateLeague, true));
    team::initialize_router(&mut router);
    router
}

/*
mod league {
    use auth::CurrentUser;
    use std::collections::HashMap;

    #[derive(RustcDecodable, Clone)]
    struct CreateLeagueRequest {
        name: String,
        sport: Option<String>
    }

    pub struct CreateLeague;

    impl ::handler::ReqAction for CreateLeague {
        type TReq = CreateLeagueRequest;

        fn validate_request(&self, request: &Self::TReq) -> Option<HashMap<String, Vec<String>>> {
            let mut sport_errors = Vec::new();
            match request.sport {
                Some(ref sport) => {
                    if sport.len() == 0 {
                        sport_errors.push("sport cannot be empty".to_string());
                    }
                },
                None => sport_errors.push("sport cannot be empty".to_string())
            }
            if sport_errors.len() > 0 {
                let mut errors = HashMap::new();
                errors.insert("sport".to_string(), sport_errors);
                Some(errors)
            } else {
                None
            }
        }

        fn authorize(&self, request: &Self::TReq, current_user: &CurrentUser) -> bool {
            current_user.user_id == 1
        }

        fn handle(&self, request: &Self::TReq, current_user: &Option<CurrentUser>) -> Option<usize> {
            println!("Sport: {:?}", request.sport);
            Some(1)
        }
    }
}
*/



