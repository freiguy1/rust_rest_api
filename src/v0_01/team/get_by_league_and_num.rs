
use ::handlers::unsecured::{ RespAction, Bundle };
use ::handlers::{ ActionError };
use std::collections::HashMap;

pub struct GetByLeagueAndNum;

impl RespAction for GetByLeagueAndNum {
    type TResp = GetByLeagueAndNumResponse;

    // Step 1
    fn validate(&self, query_params: &::router::Params) -> HashMap<&str, &str> {
        let mut errors = HashMap::new();

        if let Some(Ok(team_num)) = query_params.find("team_num").map(|q| q.parse::<usize>()) {
            if team_num == 0 {
                errors.insert("team_num", "must be an integer greater than 0");
            }
        } else {
            errors.insert("team_num", "must be an integer greater than 0");
        }

        if let Some(Ok(league_id)) = query_params.find("league_id").map(|q| q.parse::<usize>()) {
            if league_id == 0 {
                errors.insert("league_id", "must be an integer greater than 0");
            }
        } else {
            errors.insert("league_id", "must be an integer greater than 0");
        }

        errors
    }

    // Step 2
    fn handle(&self,
              bundle: Bundle) -> Result<Self::TResp, ActionError> {
        let league_id = bundle.query_params.find("league_id").unwrap().parse().unwrap();
        let num = bundle.query_params.find("team_num").unwrap().parse().unwrap();
        Ok(GetByLeagueAndNumResponse{ num: num, league_id: league_id, name: "Team 1".to_string() })
    }
}

#[derive(RustcEncodable)]
struct GetByLeagueAndNumResponse {
    num: usize,
    league_id: usize,
    name: String
}
