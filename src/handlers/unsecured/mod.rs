use rustc_serialize::Decodable;
use std::any::Any;
use iron::prelude::*;

mod req;
mod req_resp;
mod resp;

pub use self::req::{ ReqAction, ReqHandler };
pub use self::req_resp::{ ReqRespAction, ReqRespHandler };
pub use self::resp::{ RespAction, RespHandler };

pub struct BundleWithReq<TReq: Decodable + Any + Clone> {
    transfer_object: TReq,
    query_params: ::router::Params,
    current_user: Option<::auth::CurrentUser>,
    config: ::std::sync::Arc<::rustc_serialize::json::Json>
}

pub struct Bundle {
    pub query_params: ::router::Params,
    pub current_user: Option<::auth::CurrentUser>,
    pub config: ::std::sync::Arc<::rustc_serialize::json::Json>
}

fn extract_current_user(req: &Request) -> Option<::auth::CurrentUser> {
    req.extensions.get::<::auth::CurrentUser>().map(|current_user| current_user.clone())
}

